/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg413hw1;

import java.util.*;

/**
 *
 * @author Mark
 */
public class Evaluator {

    private Stack<Operand> opdStack;
    private Stack<Operator> oprStack;

    public Evaluator() {
        opdStack = new Stack<Operand>();
        oprStack = new Stack<Operator>();
    }

    public int eval(String expr) {
        String tok = "#";               // initialize tok to "#" 
        String delimiters;              // create data holder for delimiters
        Operator.initializeHashMap();   // initialize HashMap
        // init stack - necessary with operator priority schema;
        // the priority of any operator in the operator stack other then
        // the usual operators - "+-*/" - should be less than the priority
        // of the usual operators 
        oprStack.push(Operator.operators.get(tok));
        delimiters = "+-*/#! ";
        expr = expr + "!";              // add the "!" bogus operator
        StringTokenizer st = new StringTokenizer(expr, delimiters, true);
        // the 3rd arg is true to indicate to use the delimiters as tokens, too
        // but we'll filter out spaces
        while (st.hasMoreTokens()) {

            if (!(tok = st.nextToken()).equals(" ")) {      // filter out spaces 
                if (Operand.check(tok)) {                   // check if tok is an operand 
                    opdStack.push(new Operand(tok));
                } else {
                    if (!Operator.check(tok)) {
                        System.out.println("*****invalid token******");
                        System.exit(1);
                    }
                    Operator newOpr = (Operator) Operator.operators.get(tok); // POINT 1
                    /**
                     * added "&& (oprStack.peek().priority() != 1)" in the if
                     * statement to check if the stack is empty or have reached
                     * the end. priority 1 is the #
                     */
                    while (((Operator) oprStack.peek()).priority() >= newOpr.priority() && (oprStack.peek().priority() != 1)) {
                        // note that when we eval the expression 1 - 2 we will
                        // push the 1 then the 2 and then do the subtraction operation
                        // This means that the first number to be popped is the
                        // second operand, not the first operand - see the following code 
                        Operator oldOpr = ((Operator) oprStack.pop());
                        Operand op2 = (Operand) opdStack.pop();
                        Operand op1 = (Operand) opdStack.pop();
                        opdStack.push(oldOpr.execute(op1, op2));
                    }
                    oprStack.push(newOpr);
                }
            }
            // Control gets here when we've picked up all of the tokens; you must add
            // code to complete the evaluation - consider how the code given here
            // will evaluate the expression 1+2*3
            // When we have no more tokens to scan, the operand stack will contain 1 2
            // and the operator stack will have + * with 2 and * on the top;
            // In order to complete the evaluation we must empty the stacks (except
            // the init operator on the operator stack); that is, we should keep
            // evaluating the operator stack until it only contains the init operator;
            // Suggestion: create a method that takes an operator as argument and
            // then executes the while loop; also, move the stacks out of the main
            // method     
        }
        /**
         * return the last value in the stack
         */
        return opdStack.pop().getValue();
    }
}

/**
 * Operand(String tok) is a constructor for String inputs Operand(int value) is
 * a constructor for int inputs static boolean check(String tok) will check for
 * an input if it is an Operand or an Operator int getValue() will return what
 * the private datamember "value" contains
 *
 * @author Mark
 */
class Operand {

    private int Value;

    Operand(String tok) {
        this.Value = Integer.parseInt(tok);
    }

    Operand(int value) {
        this.Value = value;
    }

    public static boolean check(String tok) {
        try {
            Integer.parseInt(tok);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    int getValue() {
        return this.Value;
    }
}

/**
 * public static HashMap<String, Operator> operators = new HashMap<String,
 * Operator>() will hold the mathematical operators after calling
 * initializeMap() static void initializeMap() will initialize the
 * HashMap<String, Operator> operators with the appropriate operators abstract
 * int priority() will be used in the subclasses to return the priority of the
 * subclass public static boolean check(String tok) will check if a token is an
 * Operator or not abstract Operand execute(Operand opd1, Operand opd2) will be
 * used in the subclasses to execute the appropriate mathematical operations
 * based on the subclass
 *
 * @author Mark
 */
abstract class Operator {

    public static HashMap<String, Operator> operators = new HashMap<String, Operator>();

    static void initializeHashMap() {
        operators.put("+", new AdditionOperator());
        operators.put("-", new SubtractionOperator());
        operators.put("*", new MultiplicationOperator());
        operators.put("/", new DividisionOperator());
        operators.put("!", new BangOperator());
        operators.put("#", new HashOperator());
    }

    abstract int priority();

    public static boolean check(String tok) {
        if (operators.containsKey(tok)) {
            return true;
        } else {
            return false;
        }
    }

    abstract Operand execute(Operand opd1, Operand opd2);
}

/**
 * int priority will hold priority of the subclass String tok will hold the
 * mathematical operator int priority will return the subclass' priority value
 * which is 2 Operand execute(Operand opd1, Operand opd2) will add opd1 and opd2
 *
 * @author Mark
 */
class AdditionOperator extends Operator {

    int priority = 2;
    String tok = "+";

    int priority() {
        return this.priority;
    }

    Operand execute(Operand opd1, Operand opd2) {
        int x = opd1.getValue() + opd2.getValue();
        Operand result = new Operand(x);
        return result;
    }
}

/**
 * int priority will hold priority of the subclass String tok will hold the
 * mathematical operator int priority will return the subclass' priority value
 * which is 2 Operand execute(Operand opd1, Operand opd2) will subrtract opd2
 * from opd1
 *
 * @author Mark
 */
class SubtractionOperator extends Operator {

    int priority = 2;
    String tok = "-";

    int priority() {
        return this.priority;
    }

    public Operand execute(Operand opd1, Operand opd2) {
        int x = opd1.getValue() - opd2.getValue();
        Operand result = new Operand(x);
        return result;
    }
}

/**
 * int priority will hold priority of the subclass String tok will hold the
 * mathematical operator int priority will return the subclass' priority value
 * which is 3 Operand execute(Operand opd1, Operand opd2) will multiply opd2 and
 * opd1
 *
 * @author Mark
 */
class MultiplicationOperator extends Operator {

    int priority = 3;
    String tok = "*";

    int priority() {
        return this.priority;
    }

    public Operand execute(Operand opd1, Operand opd2) {
        int x = opd1.getValue() * opd2.getValue();
        Operand result = new Operand(x);
        return result;
    }
}

/**
 * int priority will hold priority of the subclass String tok will hold the
 * mathematical operator int priority will return the subclass' priority value
 * which is 3 Operand execute(Operand opd1, Operand opd2) will divide opd1 by
 * opd2
 *
 * @author Mark
 */
class DividisionOperator extends Operator {

    int priority = 3;
    String tok = "/";

    int priority() {
        return this.priority;
    }

    public Operand execute(Operand opd1, Operand opd2) {
        int x = opd1.getValue() / opd2.getValue();
        Operand result = new Operand(x);
        return result;
    }
}

/**
 * int priority will hold priority of the subclass String tok will hold the
 * mathematical operator int priority will return the subclass' priority value
 * which is 0 Operand execute(Operand opd1, Operand opd2) will return null
 * because it is a bogus class operator
 *
 * @author Mark
 */
class BangOperator extends Operator {

    int priority = 0;
    String tok = "!";

    int priority() {
        return this.priority;
    }

    Operand execute(Operand opd1, Operand opd2) {
        return null;
    }
}

/**
 * int priority will hold priority of the subclass String tok will hold the
 * mathematical operator int priority will return the subclass' priority value
 * which is 1 Operand execute(Operand opd1, Operand opd2) will return null
 * because this is a bogus class operator
 *
 * @author Mark
 */
class HashOperator extends Operator {

    int priority = 1;
    String tok = "#";

    int priority() {
        return this.priority;
    }

    Operand execute(Operand opd1, Operand opd2) {
        return null;
    }
}
