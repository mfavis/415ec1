/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg413ec1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.text.*;

import pkg413hw1.Evaluator;

public class CalculatorGUI extends JFrame implements ActionListener {

    static int width = 250;
    static int height = 325;
    boolean isEvaluated = false;
    boolean beginning = true;
    JButton source;
    JTextField LCD;

    public static void main(String[] args) {
        CalculatorGUI app = new CalculatorGUI();
        app.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
    }

    public CalculatorGUI() {
        super();

        JFrame mainFrame = new JFrame("Calculator");

        // create button contents
        String[] buttons = {"CE", "C", "(", ")", "7", "8",
            "9", "+", "4", "5", "6", "-", "1", "2", "3", "*", "0", ".", "=", "/"};

        // set main frame attributes
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setBounds(0, 0, width, height);

        //This will center the JFrame in the middle of the screen
        mainFrame.setLocationRelativeTo(null);

        // create two panels that will hold the interface 
        // for both lcd and buttons
        final JPanel displayPanel = new JPanel();
        final JPanel inputButtonsPanel = new JPanel();

        // create contents of displayPanel
        // and set attribute
        LCD = new JTextField("0", 15);
        LCD.setHorizontalAlignment(JTextField.RIGHT);

        displayPanel.add(LCD);

        // create buttons
        for (int i = 0; i < buttons.length; i++) {
            JButton newButton = new JButton(buttons[i]);
            newButton.addActionListener(this);
            inputButtonsPanel.add(newButton);
        }

        // layout manager
        GridLayout buttonsLayout = new GridLayout(5, 4);

        inputButtonsPanel.setLayout(buttonsLayout);

        // add panels to mainFrame
        mainFrame.add(displayPanel, BorderLayout.NORTH);
        mainFrame.add(inputButtonsPanel, BorderLayout.CENTER);

        // make sure the Window is visible
        mainFrame.setVisible(true);

        // set initial value of textfield to zero
        LCD.setText("0");
        LCD.setEditable(false);
    }

    public void showResult() {
        String newText = LCD.getText();
        // this will add the user's button clicks
        if (newText.equalsIgnoreCase("0") && beginning == true) {
            LCD.setText(source.getText());
            beginning = false;
        } else if (source.getText().equalsIgnoreCase("CE")) {
            LCD.setText("0");
            isEvaluated = false;
            beginning = true;
        } else if (source.getText().equalsIgnoreCase("C")) {
            if (newText.length() > 0) {
                newText = newText.substring(0, newText.length() - 1);
                LCD.setText(newText);
            }
        } else if (source.getText().equalsIgnoreCase("=") && isEvaluated == false) {
            Evaluator anEvaluator = new Evaluator();
            Integer result = anEvaluator.eval(LCD.getText());
            LCD.setText(result.toString());
            isEvaluated = true;
        } else if (source.getText().equalsIgnoreCase("=")) {
            // Do nothing if just isEvaluated is true
            // otherwise it will crash
        } else {
            LCD.setText(newText + source.getText());
            isEvaluated = false;
        }

    }

    public void actionPerformed(ActionEvent ae) {
        source = (JButton) ae.getSource();
        showResult();
    }
}
